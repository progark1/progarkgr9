package com.cobracombat.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.cobracombat.game.models.Config;
import com.cobracombat.game.states.GameStateManager;
import com.cobracombat.game.views.DisplayNameView;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.ScreenUtils;

public class CobraCombat extends ApplicationAdapter {
	
	//Screen dimensions, subject to change
	public static final String TITLE = "Cobra Combat";
	public static final int WIDTH = 1920;
	public static final int HEIGHT = 1080;
	public static final int PLAYABLE_WIDTH = 1750;
	public static final int PLAYABLE_HEIGHT = 1000;
	public static int SPACE_TOP = 90;
	public static int SPACE_SIDE;

	private GameStateManager gsm;
	private SpriteBatch batch;

	@Override
	public void create() {
		batch = new SpriteBatch();
		gsm = new GameStateManager();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Config.WIDTH = WIDTH;
		Config.HEIGHT = HEIGHT;
		Config.SPACE_SIDE = (WIDTH - PLAYABLE_WIDTH) / 2;
		Config.SPACE_TOP = (HEIGHT - PLAYABLE_HEIGHT) / 2;
}

	@Override
	public void render() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(batch);
	}

	@Override
	public void dispose() {
		batch.dispose();
	}
	
}
