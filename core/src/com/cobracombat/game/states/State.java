package com.cobracombat.game.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.cobracombat.game.CobraCombat;

public abstract class State {
    protected OrthographicCamera cam;
    protected Vector3 mouse;
    protected final GameStateManager gsm;

    protected State(GameStateManager gsm){
        this.gsm = gsm;
        cam = new OrthographicCamera(CobraCombat.WIDTH, CobraCombat.HEIGHT);
        cam.setToOrtho(false);
        mouse = new Vector3();

    }
    public abstract void render(SpriteBatch sb);
    public abstract void dispose();

    // handles input from the user, sends to controller
    protected abstract void handleInput();

    // updates minor changes to the models by using the update function in the models
    public abstract void update(float dt);

}