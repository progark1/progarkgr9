package com.cobracombat.game.controllers;

import com.cobracombat.game.models.ArenaModel;
import com.cobracombat.game.models.PlayerModel;

public abstract class Controller {
    PlayerModel player;
    ArenaModel arena;

    Controller(PlayerModel player, ArenaModel arena) {
        this.player = player;
        this.arena = arena;
    }

    public abstract void update(float delta);
}