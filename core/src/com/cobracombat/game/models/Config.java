package com.cobracombat.game.models;

public class Config {

    private static Config instance = new Config();
    public static int SPACE_SIDE;
    public static int SPACE_TOP;
    public static int HEIGHT;
    public static int WIDTH;
    public String username;


    private Config(){}


    public static Config getInstance(){
        return instance;
    }
}